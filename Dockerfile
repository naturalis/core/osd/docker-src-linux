FROM alpine:3.16 as first_stage

ARG PACKER_VERSION=1.8.3
ARG ANSIBLE_VERSION=4.1.0
# Needs to specify openstacksdk version as described below:
# https://storyboard.openstack.org/#!/story/2010128
# https://bugs.launchpad.net/openstack-i18n/+bug/1975497
ARG OPENSTACKSDK_VERSION=0.61

RUN apk --update --no-cache add \
    ca-certificates \
    wget \
    openssh-client \
    openssl \
    python3 \
    py3-pip \
    py3-cryptography \
    linux-headers \
    coreutils \
    sshpass 

RUN apk --update add --virtual \
    .build-deps \
    python3-dev \
    libffi-dev \
    openssl-dev \
    build-base  \
    && pip3 install \
        ansible==${ANSIBLE_VERSION} \
        ansible-lint \
        python-openstackclient \
        openstacksdk==${OPENSTACKSDK_VERSION} \
        pywinrm \
        jinja2 \
        python-hcl2 \
    && apk del .build-deps \
    && rm -rf /var/cache/apk/* \
    && find /usr/lib/ -name '__pycache__' -print0 | xargs -0 -n1 rm -rf \
    && find /usr/lib/ -name '*.pyc' -print0 | xargs -0 -n1 rm -rf

RUN update-ca-certificates

RUN wget -nv https://releases.hashicorp.com/packer/${PACKER_VERSION}/packer_${PACKER_VERSION}_linux_amd64.zip -O packer.zip
RUN mkdir /out
RUN unzip packer.zip -d /out/

RUN chmod +x /out/*


# --------------------------------------------------------------------------------------------------
# FINAL IMAGE
# --------------------------------------------------------------------------------------------------
FROM alpine:3.16

RUN set -eux \
	&& apk add --no-cache \
		git \
		python3 \
		jq \
 		bash \
		curl \
        openssh-client \
        openssl \
	&& ln -sf /usr/bin/python3 /usr/bin/python \
	&& ln -sf ansible /usr/local/bin/ansible-inventory \
	&& ln -sf ansible /usr/local/bin/ansible-playbook \
	&& ln -sf ansible /usr/local/bin/ansible-test \
    && find /usr/lib/ -name '__pycache__' -print0 | xargs -0 -n1 rm -rf \
	&& find /usr/lib/ -name '*.pyc' -print0 | xargs -0 -n1 rm -rf

RUN mkdir -p /etc/ansible \
 && echo 'localhost' > /etc/ansible/hosts \
 && echo -e """\
\n\
Host *\n\
    StrictHostKeyChecking no\n\
    UserKnownHostsFile=/dev/null\n\
""" >> /etc/ssh/ssh_config

COPY --from=first_stage /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=first_stage /out/* /usr/local/bin/
COPY --from=first_stage /usr/lib/python3.10/site-packages/ /usr/lib/python3.10/site-packages/
COPY --from=first_stage /usr/bin/ansible /usr/local/bin/ansible
COPY --from=first_stage /usr/bin/ansible-lint /usr/local/bin/ansible-lint
COPY --from=first_stage /usr/bin/ansible-connection /usr/local/bin/ansible-connection 


CMD ["/bin/bash"]
